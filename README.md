# LattesGraph 9.0

## Run

`lattesgraph.py <configuration-file.config>`

## Change log
* Migration to Python 3 

## REQUISITOS

Para a compilação precisam-se de alguns módulos Python. Para instalar esses módulos execute como root (admin):
```
$ apt-get install python-all python-setuptools python-utidylib python-matplotlib python-levenshtein python-pygraphviz python-numpy tidy python-scipy python-imaging python-mechanize
$ easy_install pytidylib
$ pip install beautifulsoup
```	

Em Ubuntu pode executar as seguintes instruções no terminal (linha de comandos):
```
$ sudo apt-get install python-all python-setuptools python-utidylib python-matplotlib python-levenshtein python-pygraphviz python-numpy tidy python-scipy python-imaging python-mechanize
$ sudo easy_install pytidylib
$ sudo pip install beautifulsoup
```

## EXECUÇÃO

Teste o scriptLattes com os seguintes dois exemplos (linha de comandos):

### EXEMPLO 01:

```
$ cd <nome_diretorio_scriptLattes>
$ python scriptLattes.py ./exemplo/teste-01.config
```

Nesse exemplo consideram-se todas as produções cujos anos de publicações estão entre 2006 e 2014. Nenhum rótulo foi considerado para os membros. 
	
Os IDs Lattes dos 3 membros está listada em: `./exemplo/teste-01.list`

O resultado da execução estará disponível em: `./exemplo/teste-01/`

### EXEMPLO 02

```
$ cd <nome_diretorio_scriptLattes>
$ python scriptLattes.py ./exemplo/teste-02.config
```

Nesse exemplo consideram-se todas as produções cadastradas nos CVs Lattes. São considerados rótulos para os membros do grupo (professor, colaborador, aluno).

Adicionalmente também são apresentadas as informações de Qualis para os artigos publicados (congressos e journals).

Os IDs Lattes dos membros está listada em: `./exemplo/teste-02.list`

O resultado da execução estará disponível em: `./exemplo/teste-02/`


## Original Project - Developers

* Jesús P. Mena-Chalco <jesus.mena@ufabc.edu.br>
* Roberto M. Cesar-Jr <cesar@vision.ime.usp.br>

## Desenvolvedor de uma nova versão

* Thiago Nascimento <nascimenthiago@gmail.com>