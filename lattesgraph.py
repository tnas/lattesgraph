#  LattesGraph 9.0
#  Copyright 2020: Thiago Nascimento
#
#  Este programa é um software livre; você pode redistribui-lo e/ou 
#  modifica-lo dentro dos termos da Licença Pública Geral GNU como 
#  publicada pela Fundação do Software Livre (FSF); na versão 2 da 
#  Licença, ou (na sua opinião) qualquer versão.
#
#  Este programa é distribuído na esperança que possa ser util, 
#  mas SEM NENHUMA GARANTIA; sem uma garantia implicita de ADEQUAÇÂO a qualquer
#  MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
#  Licença Pública Geral GNU para maiores detalhes.
#
#  Você deve ter recebido uma cópia da Licença Pública Geral GNU
#  junto com este programa, se não, escreva para a Fundação do Software
#  Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#

import warnings

from scriptLattes.grupo import *
from scriptLattes.util import *

warnings.filterwarnings('ignore')

if 'win' in sys.platform.lower():
    os.environ['PATH'] += ";" + os.path.abspath(os.curdir + '\\Graphviz2.36\\bin')

if __name__ == "__main__":
    arquivoConfiguracao = sys.argv[1]

    novoGrupo = Grupo(arquivoConfiguracao)
    novoGrupo.imprimirListaDeRotulos()

    if criarDiretorio(novoGrupo.obterParametro('global-diretorio_de_saida')):
        novoGrupo.carregarDadosCVLattes()  # obrigatorio
        novoGrupo.compilarListasDeItems()  # obrigatorio
        novoGrupo.identificarQualisEmPublicacoes()  # obrigatorio
        novoGrupo.calcularInternacionalizacao()  # obrigatorio
        novoGrupo.imprimirMatrizesDeFrequencia()

        novoGrupo.gerarGrafosDeColaboracoes()  # obrigatorio
        novoGrupo.gerarGraficosDeBarras() # java charts
        novoGrupo.gerarMapaDeGeolocalizacao() # obrigatorio
        novoGrupo.gerarPaginasWeb()  # obrigatorio
        novoGrupo.gerarArquivosTemporarios()  # obrigatorio

        # copiar imagens e css
        copiarArquivos(novoGrupo.obterParametro('global-diretorio_de_saida'))

        print("LattesGraph has been executed successfully.")

