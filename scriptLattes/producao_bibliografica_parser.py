from .producaobibliografica.producao_bibliografica import *
from enum import Enum, auto
from collections import namedtuple
from abc import ABC, abstractmethod

StruturaProducao = namedtuple('EstruturaProducao',
                              ['nome', 'dadosBasicos', 'detalhamento', 'titulo', 'ano', 'natureza'])

class TipoProducao(Enum):

    @property
    def nome(self):
        return self.value.nome

    @property
    def dadosBasicos(self):
        return self.value.dadosBasicos

    @property
    def dadosBasicos(self):
        return self.value.detalhamento

    @property
    def dadosBasicos(self):
        return self.value.titulo

    @property
    def dadosBasicos(self):
        return self.value.ano

    @property
    def natureza(self):
        return self.value.natureza

    TRABALHO_EM_EVENTOS = StruturaProducao(
        'trabalho-em-eventos', 'dados-basicos-do-trabalho', 'detalhamento-do-trabalho',
        'titulo-do-trabalho', 'ano-do-trabalho', 'natureza')

    ARTIGO_PUBLICADO = StruturaProducao(
        'artigo-publicado', 'dados-basicos-do-artigo', 'detalhamento-do-artigo',
        'titulo-do-artigo', 'ano-do-artigo', 'natureza')

    CAPITULO_LIVRO = StruturaProducao(
        'capitulo-de-livro-publicado', 'dados-basicos-do-capitulo', 'detalhamento-do-capitulo',
        'titulo-do-capitulo-do-livro', 'ano', 'tipo')

    APRESENTACAO_TRABALHO = StruturaProducao(
        'apresentacao-de-trabalho', 'dados-basicos-da-apresentacao-de-trabalho',
        'detalhamento-da-apresentacao-de-trabalho', 'titulo', 'ano', 'natureza')

    OUTRA = StruturaProducao(
        'outra-producao-bibliografica', 'dados-basicos-de-outra-producao', 'detalhamento-de-outra-producao',
        'titulo', 'ano', 'natureza')


class NaturezaTrabalhoEvento(Enum):
    COMPLETO = auto()
    RESUMO = auto()
    RESUMO_EXPANDIDO = auto()


class ProducaoBibliograficaParser:

    authorSeparator = '; '

    def __init__(self, membro, producaoBibliografica, tipoProducao):
        self.producaoBibliografica = producaoBibliografica
        self.tipoProducao = tipoProducao
        self.membro = membro

    def parse(self):

        listaPublicacoes = self.producaoBibliografica.find_all(self.tipoProducao.value.nome)

        for publicacao in listaPublicacoes:

            dadosBasicos = publicacao.find(self.tipoProducao.value.dadosBasicos)
            detalhamento = publicacao.find(self.tipoProducao.value.detalhamento)
            autoresCitacao = list()
            for autor in publicacao.find_all('autores'):
                autoresCitacao.append(autor.get('nome-para-citacao').split(';')[0])

            autores = ProducaoBibliograficaParser.authorSeparator.join(autoresCitacao)
            natureza = dadosBasicos.get(self.tipoProducao.value.natureza)
            doi = dadosBasicos.get('doi')
            titulo = dadosBasicos.get(self.tipoProducao.value.titulo)
            ano = dadosBasicos.get(self.tipoProducao.value.ano)

            paginaInicial = detalhamento.get('pagina-inicial')
            paginaFinal = detalhamento.get('pagina-final')
            numPaginas = detalhamento.get('numero-de-paginas')
            numeroPaginas = '' if numPaginas is None else numPaginas
            paginas = numeroPaginas if paginaInicial is None else paginaInicial + '-' + paginaFinal

            producao = self.parseDetalhamento(detalhamento, natureza)
            producao.setDadosBasicos(doi, titulo, natureza, ano, paginas, autores)
            self.appendMembro(producao)

    @abstractmethod
    def parseDetalhamento(self, detalhamento, natureza):
        pass

    @abstractmethod
    def appendMembro(self, producao):
        pass


class TrabalhoEmEventoParser(ProducaoBibliograficaParser):

    def __init__(self, membro, producaoBibliografica, tipoProducao):
        super().__init__(membro, producaoBibliografica, tipoProducao)
        self.natureza = None

    def parseDetalhamento(self, detalhamento, natureza):
        self.natureza = natureza
        evento = detalhamento.get('nome-do-evento')
        volume = detalhamento.get('volume')

        producao = None
        if self.natureza == NaturezaTrabalhoEvento.COMPLETO.name:
            producao = TrabalhoEmEvento(volume, evento)
        elif self.natureza == NaturezaTrabalhoEvento.RESUMO.name:
            producao = Resumo(volume, evento)
        elif self.natureza == NaturezaTrabalhoEvento.RESUMO_EXPANDIDO.name:
            producao = ResumoExpandido(volume, evento)

        return producao

    @abstractmethod
    def appendMembro(self, producao):
        if self.natureza == NaturezaTrabalhoEvento.COMPLETO.name:
            self.membro.listaTrabalhoCompletoEmCongresso.append(producao)
        elif self.natureza == NaturezaTrabalhoEvento.RESUMO.name:
            self.membro.listaResumoEmCongresso.append(producao)
        elif self.natureza == NaturezaTrabalhoEvento.RESUMO_EXPANDIDO.name:
            self.membro.listaResumoExpandidoEmCongresso.append(producao)


class ArtigoPeriodicoParser(ProducaoBibliograficaParser):

    def __init__(self, membro, producaoBibliografica, tipoProducao):
        super().__init__(membro, producaoBibliografica, tipoProducao)

    def parseDetalhamento(self, detalhamento, natureza):
        issn = detalhamento.get('issn')
        numero = detalhamento.get('serie')
        periodico = detalhamento.get('titulo-do-periodico-ou-revista')
        return ArtigoPeriodico(issn, numero, periodico)

    def appendMembro(self, producao):
        self.membro.listaArtigoEmPeriodico.append(producao)


class CapituloLivroParser(ProducaoBibliograficaParser):

    def __init__(self, membro, producaoBibliografica, tipoProducao):
        super().__init__(membro, producaoBibliografica, tipoProducao)

    def parseDetalhamento(self, detalhamento, natureza):
        livro = detalhamento.get('titulo-do-livro')
        editora = detalhamento.get('nome-da-editora')
        edicao = detalhamento.get('numero-da-edicao-revisao')
        return CapituloLivro(livro, editora, edicao)

    def appendMembro(self, producao):
        self.membro.listaCapituloDeLivroPublicado.append(producao)


class OutraProducaoParser(ProducaoBibliograficaParser):

    def __init__(self, membro, producaoBibliografica, tipoProducao):
        super().__init__(membro, producaoBibliografica, tipoProducao)

    def parseDetalhamento(self, detalhamento, natureza):
        return OutraProducao()

    def appendMembro(self, producao):
        self.membro.listaOutroTipoDeProducaoBibliografica.append(producao)

class ApresentacaoTrabalhoParser(ProducaoBibliograficaParser):

    def __init__(self, membro, producaoBibliografica, tipoProducao):
        super().__init__(membro, producaoBibliografica, tipoProducao)

    def parseDetalhamento(self, detalhamento, natureza):
        return ApresentacaoTrabalho()

    def appendMembro(self, producao):
        self.membro.listaApresentacaoDeTrabalho.append(producao)