from abc import ABC, abstractmethod


class ProducaoBibliografica(ABC):

    def __init__(self):
        super().__init__()
        self.doi = None
        self.titulo = None
        self.natureza = None
        self.ano = None
        self.paginas = None
        self.autores = None

    def setDadosBasicos(self, doi, titulo, natureza, ano, paginas, autores):
        self.doi = doi
        self.titulo = titulo
        self.natureza = natureza
        self.ano = ano
        self.paginas = paginas
        self.autores = autores

    @abstractmethod
    def getStringHeader(self):
        pass

    def __str__(self):
        s = "\n[{0}]\n".format(self.getStringHeader())
        s += "-DOI         : " + self.doi + "\n"
        s += "-Autores     : " + self.autores + "\n"
        s += "-Titulo      : " + self.titulo + "\n"
        s += "-Ano         : " + self.ano + "\n"
        s += "-Natureza    : " + self.natureza + "\n"
        s += "-Paginas     : " + self.paginas + "\n"
        return s


class TrabalhoEmEvento(ProducaoBibliografica):

    def __init__(self, volume, evento):
        super().__init__()
        self.volume = volume
        self.evento = evento

    def getStringHeader(self):
        return "TRABALHO COMPLETO PUBLICADO EM CONGRESSO"

    def __str__(self):
        s = super().__str__()
        s += "-Volume      : " + self.volume + "\n"
        s += "-Evento      : " + self.evento + "\n"
        return s


class ResumoExpandido(TrabalhoEmEvento):

    def __init__(self, volume, evento):
        super().__init__(volume, evento)

    def getStringHeader(self):
        return "RESUMO EXPANDIDO EM CONGRESSO"


class Resumo(TrabalhoEmEvento):

    def __init__(self, volume, evento):
        super().__init__(volume, evento)

    def getStringHeader(self):
        return "RESUMO EM CONGRESSO"


class ArtigoPeriodico(ProducaoBibliografica):

    def __init__(self, issn, serie, periodico):
        super().__init__()
        self.issn = issn
        self.serie = serie
        self.periodico = periodico

    def __str__(self):
        s = super().__str__()
        s += "-Numero      : " + self.serie + "\n"
        s += "-Periodico   : " + self.periodico + "\n"
        s += "-ISSN        : " + self.issn + "\n"
        return s

    def getStringHeader(self):
        return "ARTIGO EM PERIODICO"


class CapituloLivro(ProducaoBibliografica):

    def __init__(self, livro, editora, edicao):
        super().__init__()
        self.livro = livro
        self.editora = editora
        self.edicao = edicao

    def __str__(self):
        s = super().__str__()
        s += "-Livro       : " + self.livro + "\n"
        s += "-Editora     : " + self.editora + "\n"
        s += "-Edicao      : " + self.edicao + "\n"
        return s

    def getStringHeader(self):
        return "CAPITULO DE LIVRO PUBLICADO"


class OutraProducao(ProducaoBibliografica):

    def __init__(self):
        super().__init__()

    def getStringHeader(self):
        return "OUTRO TIPO DE PRODUCAO BIBLIOGRAFICA"


class ApresentacaoTrabalho(ProducaoBibliografica):

    def __init__(self):
        super().__init__()

    def getStringHeader(self):
        return "APRESENTACAO DE TRABALHO"
