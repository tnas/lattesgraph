from abc import ABC, abstractmethod


class Orientacao(ABC):

    def __init__(self):
        self.idOrientando = None
        self.nome = None
        self.tituloDoTrabalho = None
        self.ano = None
        self.instituicao = None
        self.agenciaDeFomento = None
        self.tipoDeOrientacao = None

    @abstractmethod
    def getStringHeader(self):
        pass

    def __str__(self):
        s = "\n[{0}]\n".format(self.getStringHeader())
        s += "-ID-ALUNO     : " + self.idOrientando + "\n"
        s += "-NOME         : " + self.nome + "\n"
        s += "-TITULO TRAB. : " + self.tituloDoTrabalho + "\n"
        s += "-ANO CONCLUS. : " + self.ano + "\n"
        s += "-INSTITUICAO  : " + self.instituicao + "\n"
        s += "-AGENCIA      : " + self.agenciaDeFomento + "\n"
        s += "-TIPO ORIENTA.: " + self.tipoDeOrientacao + "\n"
        return s


class OrientacaoConcluida(Orientacao):

    def getStringHeader(self):
        return 'ORIENTACAO CONCLUIDA'

