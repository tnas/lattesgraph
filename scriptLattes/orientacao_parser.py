from .producaobibliografica.orientacao import *
from collections import namedtuple
from enum import Enum

class TipoOrientacao(Enum):
    ConcluidaMestrado = 'orientacoes-concluidas-para-mestrado'
    ConcluidaDoutorado = 'orientacoes-concluidas-para-doutorado'
    ConcluidaPosDoutorado = 'orientacoes-concluidas-para-pos-doutorado'

class OrientacaoParser:

    def __init__(self, membro, producao):
        self.__membro = membro
        self.__producao = producao

    def parse(self):

        for tipoOrientacao in TipoOrientacao:
            pass



