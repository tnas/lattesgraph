from bs4 import BeautifulSoup
from .producao_bibliografica_parser import *
from .orientacao_parser import *

class Producao(Enum):
    BIBLIOGRAFICA = 'producao-bibliografica'
    TECNICA = 'producao-tecnica'
    OUTRA = 'outra-producao'

class LattesXmlParser:

    prefixUrlLattes = 'http://lattes.cnpq.br/'
    underlyingParser = 'html.parser'

    def __init__(self, membro, xmlLattes):
        self.__membro = membro
        self.__soup = BeautifulSoup(xmlLattes, LattesXmlParser.underlyingParser)
        self.parseXml()

    def parseXml(self):
        self.__membro.idLattes = self.__soup.find('curriculo-vitae').get('numero-identificador')
        self.__membro.url = LattesXmlParser.prefixUrlLattes + self.__membro.idLattes

        self.loadDadosGerais()
        self.loadProducaoBibliografica()
        self.loadOrientacoes()

    def loadDadosGerais(self):
        dadosGerais = self.__soup.find('dados-gerais')
        self.__membro.nomeCompleto = dadosGerais.get('nome-completo')
        self.__membro.nomeEmCitacoesBibliograficas = dadosGerais.get('nome-em-citacoes-bibliograficas')

        resumoCv = dadosGerais.find('resumo-cv')
        if resumoCv != None:
            self.__membro.textoResumo = resumoCv.get('texto-resumo-cv-rh')

        endereco = dadosGerais.find('endereco-profissional')
        self.__membro.enderecoProfissional = endereco.get('nome-instituicao-empresa') + \
                                             endereco.get('nome-unidade') + endereco.get('nome-orgao') + \
                                             endereco.get('logradouro-complemento') + endereco.get('cidade') + \
                                             endereco.get('pais') + endereco.get('uf') + endereco.get('cep')

    def loadProducaoBibliografica(self):

        producaoBibliografica = self.__soup.find(Producao.BIBLIOGRAFICA.value)
        producaoTecnica = self.__soup.find(Producao.TECNICA.value)

        for tipoProducao in TipoProducao:

            parser = None

            if tipoProducao == TipoProducao.TRABALHO_EM_EVENTOS:
                parser = TrabalhoEmEventoParser(self.__membro, producaoBibliografica, tipoProducao)
            elif tipoProducao == TipoProducao.ARTIGO_PUBLICADO:
                parser = ArtigoPeriodicoParser(self.__membro, producaoBibliografica, tipoProducao)
            elif tipoProducao == TipoProducao.CAPITULO_LIVRO:
                parser = CapituloLivroParser(self.__membro, producaoBibliografica, tipoProducao)
            elif tipoProducao == TipoProducao.OUTRA:
                parser = OutraProducaoParser(self.__membro, producaoBibliografica, tipoProducao)
            elif tipoProducao == TipoProducao.APRESENTACAO_TRABALHO:
                parser = ApresentacaoTrabalhoParser(self.__membro, producaoTecnica, tipoProducao)

            parser.parse()

    def loadOrientacoes(self):
        outraProducao = self.__soup.find(Producao.OUTRA.value)
        parser = OrientacaoParser(self.__membro, outraProducao)
        parser.parse()